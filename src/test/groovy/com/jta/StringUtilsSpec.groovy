package com.jta

import spock.lang.Specification

class StringUtilsSpec extends Specification {

    def "if string length is less than truncate index, should return same word"() {
        given:
        def str = "abc"

        when:
        def truncated = StringUtils.truncate(str, 5)

        then:
        str == truncated
    }


}
